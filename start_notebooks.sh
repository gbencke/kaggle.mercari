#!/bin/bash

cd notebook
jupyter nbextension enable beakerx --py --sys-prefix   
jupyter notebook --ip='*'
