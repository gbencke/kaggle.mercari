"""
    <TODO>
"""
import pandas as pd


def create_pickle(filename):
    """
       <TODO>
    """
    data_frame = pd.read_csv("../data/input/{}.tsv".format(filename), sep='\t')
    print data_frame.head
    data_frame.to_pickle("../data/output/{}.pickle".format(filename))


if __name__ == '__main__':
    print "create pickling for train..."
    create_pickle("train")
    print "create pickling for test..."
    create_pickle("test")
